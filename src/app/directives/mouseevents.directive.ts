import { Directive, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[mouseevent]'
})
export class MouseEventsDirective {
  @Output() mouseWheelUp = new EventEmitter();
  @Output() mouseWheelDown = new EventEmitter();
  @Output() mouseEnter = new EventEmitter();
  @Output() mouseLeave = new EventEmitter();
  constructor() { }
  @HostListener('mousewheel', ['$event']) onmousewheel(event: any) {
    this.mouseWheelFunc(event);
  }

  @HostListener('mouseenter', ['$event']) onmouseenter(event: any) {
    this.mouseEnter.emit(event);
  }

  @HostListener('mouseleave', ['$event']) onmouseleave(event: any) {
    this.mouseLeave.emit(event);
  }

  mouseWheelFunc(event: any) {
    var event = window.event || event;
    var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
    if (delta > 0) {
      this.mouseWheelUp.emit(event);
    } else if (delta < 0) {
      this.mouseWheelDown.emit(event);
    }
  }

}
