import { Input, EventEmitter, Output, Renderer, SimpleChanges } from "@angular/core";
import { OnChanges } from "@angular/core/src/metadata/lifecycle_hooks";

export class EditableControl implements OnChanges {
    @Input() value: string;
    @Output('save') onSave = new EventEmitter(false);
    @Output('cancel') onCancel = new EventEmitter(false);
    private editable: boolean = false;
    private originalValue;
    constructor(protected renderer: Renderer) {}

    public ngOnChanges(changes: SimpleChanges): void {
        if(changes.value){
            this.originalValue = changes.value.currentValue;
        }
    }

    protected mouseEnter(event) {
        this.renderer.setElementClass(event.currentTarget, "edit-border", true);
        this.renderer.setElementClass(event.currentTarget.children[0], "hidden", false);
    }

    protected mouseLeave(event) {
        this.renderer.setElementClass(event.currentTarget, "edit-border", false);
        this.renderer.setElementClass(event.currentTarget.children[0], "hidden", true);
    }

    protected click() {
        this.editable = true;
    }

    protected onChange(value) {
        this.value = value;
    }

    protected cancel() {
        this.value = this.originalValue;
        this.editable = false;
        this.onCancel.emit();
    }

    protected save() {
        this.editable = false;
        this.onSave.emit(this.value);
    }
}