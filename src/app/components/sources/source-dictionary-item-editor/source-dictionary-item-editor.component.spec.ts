import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceDictionaryItemEditorComponent } from './source-dictionary-item-view.component';

describe('SourceDictionaryItemEditorComponent', () => {
  let component: SourceDictionaryItemEditorComponent;
  let fixture: ComponentFixture<SourceDictionaryItemEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceDictionaryItemEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceDictionaryItemEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
