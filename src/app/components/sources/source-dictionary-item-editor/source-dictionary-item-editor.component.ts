import { Component, OnInit, OnChanges, Input, SimpleChanges, Renderer } from '@angular/core';
import { SourceDictionary, SourceDictionaryItem } from '../../../models';
import { Settings, SourceEditStatus } from '../../../constants';
import { ContextService } from '../../../services/context.service';

@Component({
  selector: 'app-source-dictionary-item-editor',
  templateUrl: './source-dictionary-item-editor.component.html',
  styleUrls: ['./source-dictionary-item-editor.component.css']
})
export class SourceDictionaryItemEditorComponent implements OnInit, OnChanges {
  @Input('dictionary') dictionary: SourceDictionary;
  @Input('highlightFilter') highlightFilter: string;
  @Input('dictionaryItem') dictionaryItem: SourceDictionaryItem;
  private statuses = [
    { value: SourceEditStatus.All, title: SourceEditStatus.All },
    { value: SourceEditStatus.Raw, title: SourceEditStatus.Raw },
    { value: SourceEditStatus.Progress, title: SourceEditStatus.Progress },
    { value: SourceEditStatus.Done, title: SourceEditStatus.Done }
  ];
  constructor(private contextService: ContextService<SourceDictionary>) { }
  ngOnInit() {

  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dictionary) {
      this.dictionary = changes.dictionary.currentValue;
    }
    if (changes.dictionaryItem) {
      this.dictionaryItem = changes.dictionaryItem.currentValue;
    }
    if (changes.highlightFilter) {
      this.highlightFilter = changes.highlightFilter.currentValue;
    }
  }

  private saveTitle(value) {
    this.dictionaryItem.title = value;
  }

  private saveBody(value) {
    this.dictionaryItem.body = value;
  }

  private saveStatus(value) {
    this.dictionaryItem.status = value;
  }
}
