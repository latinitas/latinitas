import { Component, OnInit, Injector, SimpleChanges } from '@angular/core';
import { Observable } from "rxjs/Rx";
import 'rxjs/add/operator/do';
import { SourceDictionary } from '../../../models/source-dictionary';
import { ContextService } from '../../../services/context.service';
import { Mapper } from '../../../helpers/mapper';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { Settings} from '../../../constants/index';
import { SourceDictionaryItem } from '../../../models/index';

@Component({
  selector: 'app-source-dictionary-page',
  templateUrl: './source-dictionary-page.component.html',
  styleUrls: ['./source-dictionary-page.component.css']
})
export class SourceDictionaryPageComponent implements OnInit {

  private dictionary: SourceDictionary;
  private dictionaryItem: SourceDictionaryItem;
  private highlightFilter: string;
  constructor(private contextService: ContextService<SourceDictionary>) {
    
  }

  ngOnInit() {
    this.contextService.cacheService.sourceDictionaries.subscribe(res=> this.dictionary = res.filter(i=>i.code === Settings.Default.SDCode)[0]);
  }

  private dictionaryChange(item: SourceDictionary) {
    this.dictionary = item;
    this.dictionaryItem = undefined;
  }

  private dictionaryItemChange(item: SourceDictionaryItem){
    this.dictionaryItem = item;
  }

  private highlightFilterChange(item: string){
    this.highlightFilter = item;
  }

 

  private save(item: SourceDictionaryItem){

  }
    
}
