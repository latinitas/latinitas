import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceDictionaryPageComponent } from './source-dictionary-page.component';

describe('SourceDictionaryPageComponent', () => {
  let component: SourceDictionaryPageComponent;
  let fixture: ComponentFixture<SourceDictionaryPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceDictionaryPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceDictionaryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
