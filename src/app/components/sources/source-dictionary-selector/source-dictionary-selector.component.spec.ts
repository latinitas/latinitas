import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceDictionarySelectorComponent } from './source-dictionary-selector.component';

describe('SourceDictionaryEditorComponent', () => {
  let component: SourceDictionarySelectorComponent;
  let fixture: ComponentFixture<SourceDictionarySelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceDictionarySelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceDictionarySelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
