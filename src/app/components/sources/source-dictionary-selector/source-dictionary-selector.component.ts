import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ContextService } from '../../../services/context.service';
import { SourceDictionary } from '../../../models';
import { Mapper } from '../../../helpers/mapper';
import { Settings } from '../../../constants/index';

@Component({
  selector: 'app-source-dictionary-selector',
  templateUrl: './source-dictionary-selector.component.html',
  styleUrls: ['./source-dictionary-selector.component.css']
})
export class SourceDictionarySelectorComponent implements OnInit {
  @Output('dictionaryChange') onDictionaryChange = new EventEmitter(false);

  private dictionaries: Array<SourceDictionary>;
  private model: SourceDictionary;
  constructor(private contextService: ContextService<SourceDictionary>) { }

  ngOnInit() {
    this.contextService.cacheService.sourceDictionaries.subscribe(res => {
      this.dictionaries = res;
      this.model = this.contextService.cacheService.sourceDictionaryCurrent = res.filter(i=>i.code === Settings.Default.SDCode)[0];
    });
  };

  private onChange() {
    this.contextService.cacheService.sourceDictionaryCurrent = this.model;
    this.onDictionaryChange.emit(this.model);
  }

  private Create() {
    
  }

  private Edit() {
    
  }

  private Delete() {
    
  }

}
