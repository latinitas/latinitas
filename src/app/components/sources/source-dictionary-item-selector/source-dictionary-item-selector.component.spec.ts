import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceDictionaryItemSelectorComponent } from './source-dictionary-item-selector.component';

describe('SourceDictionaryItemSelectorComponent', () => {
  let component: SourceDictionaryItemSelectorComponent;
  let fixture: ComponentFixture<SourceDictionaryItemSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceDictionaryItemSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceDictionaryItemSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
