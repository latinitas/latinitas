import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter, HostListener, Renderer, OnChanges, OnDestroy } from '@angular/core';
import { SourceDictionary } from '../../../models';
import { ContextService } from '../../../services/context.service';
import { Mapper } from '../../../helpers/mapper';
import { SourceDictionaryItemFilter } from '../../../models/source-dictionary-item-filter';
import { SourceDictionaryItem } from '../../../models/source-dictionary-item';
import { SourceEditStatus, Settings} from '../../../constants';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-source-dictionary-item-selector',
  templateUrl: './source-dictionary-item-selector.component.html',
  styleUrls: ['./source-dictionary-item-selector.component.css']
})
export class SourceDictionaryItemSelectorComponent implements OnInit, OnChanges, OnDestroy {

  @Input('dictionary') dictionary: SourceDictionary;
  @Output('dictionaryItemChange') onDictionaryItemChange = new EventEmitter(false);
  @Output('highlightFilterChange') onHighlightFilterChange = new EventEmitter(false);

  private sourceDictionaryItems: Array<SourceDictionaryItem>;
  private filterModel: SourceDictionaryItemFilter;
  private currentSourceDictionaryItem: SourceDictionaryItem;
  private searchTextPlaceHolder = Settings.Default.SearchTitlePlaceHolder;
  private sourceDictionaryItemsSubscription: Subscription;
  private sourceDictionaryItemsCountSubscription: Subscription;

  constructor(private contextService: ContextService<SourceDictionaryItem>, private renderer: Renderer) { }

  ngOnInit() {

  }
  ngOnDestroy(): void {
    this.sourceDictionaryItemsCountSubscription.unsubscribe();
    this.sourceDictionaryItemsSubscription.unsubscribe();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dictionary && changes.dictionary.currentValue) {
      this.dictionary = changes.dictionary.currentValue;
      this.initFilter();
      this.applyFilter();
    }
  }

  private initFilter() {
    this.filterModel = new SourceDictionaryItemFilter(Settings.Default.SDILoadingStep, 0);
    this.filterModel.getTotal = true;
    this.filterModel.count = Settings.Default.SDILoadingStep;
    this.filterModel.sourceDictionary = this.dictionary.id;
  }

  private changeStatus(event) {
    if (event.currentTarget.value !== SourceEditStatus.All) {
      this.filterModel.status = event.currentTarget.value;
      this.applyFilter();
    }
  }

  private changeSearchText(event) {
    if (this.searchTextPlaceHolder === Settings.Default.SearchBodyPlaceHolder) {
      if (event.currentTarget.value.length > 0) {
        this.initFilter();
        this.filterModel.body = event.currentTarget.value;
      } else {
        this.filterModel.body = undefined;
      }
      this.onHighlightFilterChange.emit(this.filterModel.body);
    }
    if (this.searchTextPlaceHolder === Settings.Default.SearchTitlePlaceHolder) {
      if (event.currentTarget.value.length > 0) {
        this.initFilter();
        this.filterModel.title = event.currentTarget.value;
      } else {
        this.filterModel.title = undefined;
      }

    }
    this.applyFilter();
  }

  private searchModeChange(event) {
    if (event.currentTarget.checked) {
      this.searchTextPlaceHolder = Settings.Default.SearchBodyPlaceHolder;
    } else {
      this.searchTextPlaceHolder = Settings.Default.SearchTitlePlaceHolder;
    }
    this.applyFilter();
  }

  private applyFilter() {
    if (this.filterModel.total === undefined) {
      this.getCount();
    } else {
      this.getRows();
    }
  }
  private getCount() {
    let filter = JSON.stringify(this.filterModel);
    this.sourceDictionaryItemsCountSubscription = this.contextService.storageService.getCountByParams(new SourceDictionaryItem(), filter).subscribe(
      count => this.setCount(count)
    )
  }
  private setCount(rowCount: number) {
    this.filterModel.total = rowCount;
    this.getRows();
  }
  private getRows() {
    this.filterModel.getTotal = false;
    let filter = JSON.stringify(this.filterModel);
    this.sourceDictionaryItemsSubscription = this.contextService.storageService.getByParams(new SourceDictionaryItem(), filter).subscribe(
      rows => this.sourceDictionaryItems = rows.map(i => Mapper.ToSourceDictionaryItem(i))
    )
  }

  private mouseWheelUp(event) {
    if (this.filterModel.skip > 0) {
      let rest = (this.filterModel.skip + this.filterModel.count) - Settings.Default.SDILoadingStep;
      this.filterModel.skip -= rest <  Settings.Default.SDILoadingStep ? rest : Settings.Default.SDILoadingStep;
      this.applyFilter();
    }
  }

  private mouseWheelDown(event) {
    if ((this.filterModel.count + this.filterModel.skip) < this.filterModel.total) {
      let rest = this.filterModel.total - (this.filterModel.count + this.filterModel.skip);
      this.filterModel.skip += rest < Settings.Default.SDILoadingStep ? rest : Settings.Default.SDILoadingStep;
      this.applyFilter();
    }
  }

  private sourceDictionaryItemClick(item) {
    this.currentSourceDictionaryItem = item;
    this.onDictionaryItemChange.emit(item);
  }

  private create() {
    this.currentSourceDictionaryItem = new SourceDictionaryItem();
    this.currentSourceDictionaryItem.sourceDictionaryId = this.dictionary.id;
    this.onDictionaryItemChange.emit(this.currentSourceDictionaryItem);
  }

  private upload() {

  }

}
