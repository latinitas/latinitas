import { Component, OnInit, Input, Renderer, Output, EventEmitter } from '@angular/core';
import { EditableControl } from '../../base/editable-control';

@Component({
  selector: 'app-editable-textbox',
  templateUrl: './editable-textbox.component.html',
  styleUrls: ['./editable-textbox.component.css']
})
export class EditableTextboxComponent extends EditableControl implements OnInit {

  constructor(renderer: Renderer) {
    super(renderer);
  }

  ngOnInit() {
  }

}
