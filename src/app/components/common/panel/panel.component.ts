import { Component, OnInit, Input, ViewChild, ElementRef, Renderer } from '@angular/core';
import { Animation } from '../../../helpers/animation';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css'],
  animations: [
    Animation.slideInOut,
    Animation.fadeInOut
  ]
})
export class PanelComponent implements OnInit {
  @Input() title: string;
  @Input() collapsible: boolean = false;
  @Input() collapsed: boolean = false;
  @Input() showFooter: boolean = false;
  @Input() scrollable: boolean = false;
  @ViewChild('panelheaderid') headerElement: ElementRef;
  @ViewChild('panelbodyid') bodyElement: ElementRef;
  @ViewChild('bodycontent') bodycontentElement: ElementRef;
  @ViewChild('panelfooterid') footerElement: ElementRef;
  constructor(private renderer: Renderer) { }

  ngOnInit() {
    if (this.collapsible) {
      this.renderer.setElementClass(this.headerElement.nativeElement, 'panel-collapsible-cursor', true);
    } else {
      this.renderer.setElementClass(this.headerElement.nativeElement, 'panel-collapsible-cursor', false);
    }
    if (this.collapsed) {
      this.setCollapseState(true);
    } else {
      this.setCollapseState(false);
    }
  }
  onHeadingClick() {
    if (this.collapsible) {
      this.collapsed = !this.collapsed;
      let index = this.bodyElement.nativeElement.className.indexOf('panel-body-collapsed');
      if (~index) {
        this.setCollapseState(false);
      } else {
        this.setCollapseState(true);
      }
    }
  }

  private setCollapseState(state: boolean) {
    this.renderer.setElementClass(this.bodyElement.nativeElement, 'panel-body-collapsed', state);
    if (this.footerElement) {
      this.renderer.setElementClass(this.footerElement.nativeElement, 'panel-footer-collapsed', state);
    }
  }

  public isCollapsed(): boolean {
    if (this.collapsible) {
      return this.collapsed;
    }
  }

}
