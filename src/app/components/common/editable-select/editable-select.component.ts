import { Component, OnInit, Input, Output, Renderer, EventEmitter } from '@angular/core';
import { EditableControl } from '../../base/editable-control';

@Component({
  selector: 'app-editable-select',
  templateUrl: './editable-select.component.html',
  styleUrls: ['./editable-select.component.css']
})
export class EditableSelectComponent extends EditableControl implements OnInit {
  @Input() items = [];
  constructor(renderer: Renderer) {
    super(renderer);
 }

  ngOnInit() {
  }

}
