import { Component, OnInit, Input, Renderer, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { EditableControl } from '../../base/editable-control';

@Component({
  selector: 'app-editable-textarea',
  templateUrl: './editable-textarea.component.html',
  styleUrls: ['./editable-textarea.component.css']
})
export class EditableTextareaComponent extends EditableControl implements OnInit {
  @Input() highlightFilter: string;
  constructor(renderer: Renderer) {
    super(renderer);
  }

  ngOnInit() {
  }

}
