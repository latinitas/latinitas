import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../models/menu-item';
import { Guid } from '../../helpers/guid';
import { MenuGroup } from '../../models/menu-group';
import { Menu } from '../../models/menu';
import { MenuItemType } from '../../constants/menu-item-type.enum';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  private menu: Menu;
  private collapsed = false;
  constructor() { }

  ngOnInit() {
    this.createHeader();
  }

  private createHeader() {
    this.menu = new Menu();
    this.menu.title = 'Latinitas';
    this.menu.icon = 'glyphicon glyphicon-education';
    this.menu.menuGroups = new Array<MenuGroup>(
      new MenuGroup(Guid.newGuid(), 'Fontes', new Array<MenuItem>(
        new MenuItem(Guid.newGuid(), MenuItemType.Link, 'Indeces Vocabulorum', '/source-dictionaries'),
        new MenuItem(Guid.newGuid(), MenuItemType.Divider),
        new MenuItem(Guid.newGuid(), MenuItemType.Link, 'Auctores et Opera', '/auctores')
      )),
      new MenuGroup(Guid.newGuid(), 'Enchiridii', new Array<MenuItem>(
        new MenuItem(Guid.newGuid(), MenuItemType.Link, 'Definitiones grammaticales', '/definitions'),
        new MenuItem(Guid.newGuid(), MenuItemType.Divider),
        new MenuItem(Guid.newGuid(), MenuItemType.Link, 'Formulae flectendi', '/templates')
      )),
      new MenuGroup(Guid.newGuid(), 'Indeces Vocabulorum', new Array<MenuItem>(
        new MenuItem(Guid.newGuid(), MenuItemType.Link, 'Index Latinus', '/lexemes'),
        new MenuItem(Guid.newGuid(), MenuItemType.Link, 'Index Rossicus', '/lexemes'),
        new MenuItem(Guid.newGuid(), MenuItemType.Divider),
        new MenuItem(Guid.newGuid(), MenuItemType.Link, 'Textus', '/text'),
      )));
  }

  toggled(item: any) {

  }
}
