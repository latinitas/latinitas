export const Settings = {
    Default: {
        API_URL:'http://localhost:1337/api/',
        IconPrefix : 'glyphicon glyphicon-',
        SelectPlaceHolder: 'Select ...',
        SearchTitlePlaceHolder: 'Title ...',
        SearchBodyPlaceHolder: 'Article ...',
        SDCode:'LR_KORDVOR',
        SDILoadingStep: 18
    }   
}