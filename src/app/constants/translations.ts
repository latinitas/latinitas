export const translations = {
    raw: {
        rus:'новый',
        lat:'novum'
    },
    progress:{
        rus:'в обработке',
        lat:'in profectu'
    },
    done:{
        rus:'готов',
        lat:'factum'
    }
}