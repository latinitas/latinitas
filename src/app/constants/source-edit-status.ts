export const SourceEditStatus = {
    All:'all',
    Raw: 'raw',
    Progress:'progress',
    Done: 'done'
}