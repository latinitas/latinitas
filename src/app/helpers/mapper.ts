import {
    SourceDictionary,
    SourceDictionaryItem
} from "../models";

export class Mapper {
    static ToSourceDictionary(type: any): SourceDictionary {
        let sd = new SourceDictionary();
        sd.id = type._id;
        sd.title = type.title;
        sd.code = type.code;
        sd.description = type.description;
        return sd;
    };
    static ToSourceDictionaryItem(type: any): SourceDictionaryItem {
        let sourceDictionaryItem = new SourceDictionaryItem();
        sourceDictionaryItem.id = type._id;
        sourceDictionaryItem.body = type.body;
        sourceDictionaryItem.sourceDictionaryId = type.sourceDictionary._id;
        sourceDictionaryItem.status = type.status;
        sourceDictionaryItem.title = type.title;
        return sourceDictionaryItem;
    }
}