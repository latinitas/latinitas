import { trigger, state, transition, animate, style } from '@angular/core';
export class Animation {
    
    public static slideInOut = trigger('slideInOut', [
        state('true', style({ 'height': '0px' })),
        state('false', style({ 'height': '*' })),
        transition('1 => 0', animate('500ms ease-in')),
        transition('0 => 1', animate('500ms ease-out'))
    ]);
    
    public static collapseMenu = trigger('collapseMenu', [
        state('true', style({ 'width': '50px' })),
        state('false', style({ 'width': '*' })),
        transition('1 => 0', animate('500ms ease-in')),
        transition('0 => 1', animate('500ms ease-out'))
    ]);

    public static fadeInOut = trigger('fadeInOut', [
        state('true', style({ 'opacity': '0' })),
        state('false', style({ 'opacity': '*' })),
        transition('1 => 0', animate('500ms ease-in')),
        transition('0 => 1', animate('500ms ease-out'))
    ]);
}
