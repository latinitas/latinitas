import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SourceDictionaryPageComponent } from './components/sources/source-dictionary-page/source-dictionary-page.component';
import { CacheService, RestoreService, LogService, StorageService } from './services';
import { Settings } from './constants';
import { SourceDictionaryItemSelectorComponent } from './components/sources/source-dictionary-item-selector/source-dictionary-item-selector.component';
import { SourceDictionaryItemEditorComponent } from './components/sources/source-dictionary-item-editor/source-dictionary-item-editor.component';
import { SourceDictionarySelectorComponent } from './components/sources/source-dictionary-selector/source-dictionary-selector.component';
import { ContextService } from './services/context.service';
import { HighlightPipe, SanitizePipe, TranslatePipe } from './pipes';
import { MouseEventsDirective } from './directives/mouseevents.directive';
import { EditableTextboxComponent } from './components/common/editable-textbox/editable-textbox.component';
import { PanelComponent } from './components/common/panel/panel.component';
import { EditableSelectComponent } from './components/common/editable-select/editable-select.component';
import { EditableTextareaComponent } from './components/common/editable-textarea/editable-textarea.component';

const appRoutes: Routes = [
  { path: 'source-dictionaries', component: SourceDictionaryPageComponent }
  // { path: 'definitions', component: GrammarCategoryListComponent },
  // { path: 'templates', component: TplListComponent },
  // { path: 'lexemes', component: LexemeListComponent },
  // { path: 'text', component: TextComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SourceDictionaryPageComponent,
    SourceDictionaryItemSelectorComponent,
    SourceDictionaryItemEditorComponent,
    SourceDictionarySelectorComponent,
    HighlightPipe,
    SanitizePipe,
    TranslatePipe,
    MouseEventsDirective,
    EditableTextboxComponent,
    PanelComponent,
    EditableSelectComponent,
    EditableTextareaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot()
  ],
  providers: [
    ContextService,
    StorageService,
    CacheService,
    RestoreService,
    LogService,
    { provide: Settings.Default.API_URL, useValue: "http://localhost:1337/api/" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
