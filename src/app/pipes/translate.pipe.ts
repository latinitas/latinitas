import {Pipe, PipeTransform} from '@angular/core';
import { translations } from '../constants';

@Pipe({ name: 'translate' })
export class TranslatePipe implements PipeTransform {

    transform(input: string, lang: string): any {
        return translations[input][lang] ? translations[input][lang] : input;
    }
}