import { MenuItem } from "./menu-item";
import { Model } from "./base/model";

export class MenuGroup implements Model {
    type = 'menu-group';
    constructor(
        public id: string,
        public title: string,
        public menuItems: Array<MenuItem>,
        public desc?:string,
        public icon?:string
    ) {
    }
}
