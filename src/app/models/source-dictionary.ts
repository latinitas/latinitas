import { Model } from "./index";

export class SourceDictionary implements Model {
    public id: string;
    public type: string = "source-dictionary";
    public title?: string;
    public code?: string;
    public description?: string;
}