import { MenuGroup } from "./menu-group";

export class Menu{
    title: string;
    icon:string;
    menuGroups: Array<MenuGroup>
}