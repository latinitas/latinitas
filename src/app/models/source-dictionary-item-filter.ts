export class SourceDictionaryItemFilter {
    constructor(
        public count:number,
        public skip:number,
        public sourceDictionary?: string,
        public title?:string,
        public body?: string,
        public status?: string,
        public total?:number,
        public getTotal?:boolean
    ) {}
}