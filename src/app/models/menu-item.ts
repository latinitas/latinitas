import { MenuItemType } from '../constants/menu-item-type.enum';
import { Model } from './base/model';

export class MenuItem implements Model {
    type: string = 'menu-item';
    constructor(
        public id: string,
        public menuItemType: MenuItemType,
        public title?: string,
        public href?: string
    ) { }
}
