export interface IDictionary {
    id: string;
    title: string;
    selected:boolean;
    desc?: string;
}