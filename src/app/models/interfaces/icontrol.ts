export interface IControl {
    id: string;
    title: string;
    enabled: boolean;
    class: string;
    controlName: string;
}