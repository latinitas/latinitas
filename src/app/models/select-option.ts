import { IDictionary } from "./index";
import { Guid } from "../helpers/guid";

export class SelectOption implements IDictionary{
    public id: string = Guid.newGuid();
    constructor(
        public title: string,
        public selected: boolean = false,
        public desc?: string
    ){}
   
}