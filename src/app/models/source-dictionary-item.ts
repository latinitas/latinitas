import { Model, SourceDictionary } from "./index";

export class SourceDictionaryItem implements Model {
    public id: string;
    public type: string = "source-dictionary-item";
    public sourceDictionaryId: string;
    public title: string;
    public body: string;
    public status: string;
    constructor(
        
    ) { }
}