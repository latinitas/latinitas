import { Injectable, Injector } from '@angular/core';
import { StorageService, RestoreService, LogService, CacheService } from '.';
import { Model } from '../models';

@Injectable()
export class ContextService<T extends Model> {
  public storageService: StorageService<T>;
  public restoreService: RestoreService<T>;
  public logService: LogService;
  public cacheService: CacheService;
  constructor(injector: Injector) {
    this.logService = injector.get(LogService);
    this.restoreService = injector.get(RestoreService);
    this.storageService = injector.get(StorageService);
    this.cacheService = injector.get(CacheService);
  }

}
