import { Injectable, Inject } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/Rx';
import { Settings } from '../constants/settings';
import { Model } from '../models/base/model';

@Injectable()
export class StorageService<T extends Model> {
    private headers = new Headers();
    constructor(private http: Http, @Inject(Settings.Default.API_URL) private url) { 
        
    }
    ngOnInit() {
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    }

    gets<T extends Model>(item: T) {
        return this.http.get(this.url + item.type)
            .map(res => <T[]>res.json());
    };

    getByParams<T extends Model>(item: T, query: string) {
        return this.http.get(this.url + item.type + '?params=' + query)
            .map(res => <T[]>res.json());
    };

    getCountByParams<T extends Model>(item: T, query: string) {
        return this.http.get(this.url + item.type + '?params=' + query)
            .map(res => res.json());
    };

    get<T extends Model>(item: T) {
        return this.http.get(this.url + item.type + '/' + item.id)
            .map(res => <T>res.json());
    };

    remove<T extends Model>(item: T) {
        return this.http.delete(this.url + item.type + '/' + item.id,
            { headers: this.headers })
            .map(res => res.json());
    };

    save<T extends Model>(item: T) {
        if (item.id !== undefined) {
            return this.http.put(this.url + item.type + '/' + item.id, item,
                { headers: this.headers })
                .map(res => <T>res.json());
        } else {
            return this.http.post(this.url + item.type, item,
                { headers: this.headers })
                .map(res => <T>res.json());
        }
    };
    patch<T extends Model>(item: T) {
        return this.http.patch(this.url + item.type + '/' + item.id, item,
            { headers: this.headers })
            .map(res => <T>res.json());
    };

};
