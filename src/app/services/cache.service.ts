import { Injectable } from '@angular/core';
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Observable } from "rxjs/Observable";
import { StorageService } from "./storage.service";
import { SourceDictionary } from '../models/source-dictionary';
import { Mapper } from '../helpers/mapper';
import { Settings } from '../constants/index';
@Injectable()
export class CacheService {

  private sourceDictionaryData = new ReplaySubject<SourceDictionary[]>(1);
  public sourceDictionaries: Observable<SourceDictionary[]> = this.sourceDictionaryData.asObservable();
  public sourceDictionaryCurrent: SourceDictionary;
  constructor(private sourceDictionaryStorage: StorageService<SourceDictionary>) {
    this.loadSourceDictionaries();
  }

  public loadSourceDictionaries() {
    this.sourceDictionaryStorage.gets(new SourceDictionary()).subscribe(res => {
      this.sourceDictionaryData.next(res.map(i => {
        return Mapper.ToSourceDictionary(i);
      }));
    });
  }
}
