import { Injectable } from "@angular/core";
import { Model } from "../models/base/model";

@Injectable()
export class RestoreService<T extends Model> {
  private originalItem: T;
  set(target:T, item: T) {
    this.originalItem = Object.assign(target, item);
  }
  get(): T {
    return this.originalItem;
  }
}
