import { Injectable } from '@angular/core';

@Injectable()
export class LogService {

   public logError(error: any): string {
       if(error){
           let er = Date.now() + ': ' + JSON.stringify(error);
           console.error(er);
           return er;
       }
    }

    public log(type: string, message: string): string{
        if(message){
            let lg = Date.now() + ': ' + type.toUpperCase() + ': ' + message;
            console.log(lg);
            return lg;
        }
    }
}