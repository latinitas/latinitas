import { TestBed, inject } from '@angular/core/testing';

import { ServiceContainerService } from './service-container.service';

describe('ServiceContainerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServiceContainerService]
    });
  });

  it('should be created', inject([ServiceContainerService], (service: ServiceContainerService) => {
    expect(service).toBeTruthy();
  }));
});
